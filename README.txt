KNN classifier of handwritten digits written during F# coding dojo meetup in NYC
Demonstrates certain nice features of F#:
* succinctness
* higher order & partially applied functions
* built-in data structures
* sequence expressions & splicing
* easy parallelization
* scripting!
-----------------------------------------------------------------------------------

Simple K-Nearest neighbors classifier for hand-written digit recognition
Sample data sets: 5000 trainings samples & 500 validation samples
Each sample represented as a label in the first column followed
by a contiguous array of all the gray pixels values in the image

You may modify the value of K within the script and observe the results
On this test data set, at K = 3, accuracy = 94%

Usage: fsi --exec [path-to-script] [path-to-test-set] [path-to-validation-set]
e.g.: fsi --exec knn_classify.fsx traningsample.csv validationsample.csv

P.S: 
you can use the (showDigit) function to display what a sample image might look like
