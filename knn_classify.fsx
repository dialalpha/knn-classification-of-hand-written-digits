﻿open System
open System.Drawing
open System.Windows.Forms


let k = 3

let showDigit (data: int32 array) = 
  let bmp = new Bitmap(28, 28)
  for i in 0 .. 783 do
    let value = data.[i]
    let color = Color.FromArgb(value, value, value)
    bmp.SetPixel(i % 28, i / 28, color)
  let form = new Form(Visible = true, ClientSize = Size(280, 280))
  let img = new PictureBox(Image = bmp, Dock = DockStyle.Fill, SizeMode = PictureBoxSizeMode.StretchImage)
  form.Controls.Add(img)
  form.TopMost <- true


/// Read a CSV file that begins with a descriptive header as first line
/// It is the caller's responsibility to provide exception handling
/// as well as ensure that the file is correctly formatted
let readCsvFile path =
    [|
        for str in (System.IO.File.ReadAllLines(path).[1..]) ->
            str.Split([|' '; ','|], StringSplitOptions.RemoveEmptyEntries)
            |> Array.Parallel.map (fun pixelValue -> int pixelValue)
    |]
    

/// Depending on size of images, you may want to watch out for int overflow
let distFunc img1 img2 =
    Array.map2 (fun pixel1 pixel2 -> pown (pixel1 - pixel2) 2) img1 img2
    |> Seq.skip 1 |> Seq.sum |> (float32) |> sqrt


let predictorFunc trainingSet unknown = 
    Array.sortBy (distFunc unknown) trainingSet 
    |> Seq.take k |> Seq.countBy (fun sample -> sample.[0]) |> Seq.maxBy snd |> fst
    

let validationFunc trainingSet validationSet =
    validationSet
    |> Array.Parallel.map (predictorFunc trainingSet)
    |> Array.Parallel.mapi (fun i prediction -> prediction = validationSet.[i].[0])
    |> Array.filter (fun correct -> correct = true)
    |> Array.length
    |> (fun truePredictions ->
            if truePredictions > 0 then
                float32 truePredictions / float32 validationSet.Length
            else 0.0f
    )
    
let trainingSet = 
    try readCsvFile fsi.CommandLineArgs.[1]
    with ex -> failwith (String.Format("Failed reading training data: \n{0}", ex.Message))
let validationSet = 
    try readCsvFile fsi.CommandLineArgs.[2]
    with ex -> failwith (String.Format("Failed reading validation data: \n{0}", ex.Message))

printfn "Running classifier ... this might take some time ..."
let accuracy = validationFunc trainingSet validationSet
printfn "Done. Prediction accuracy: %f" accuracy 